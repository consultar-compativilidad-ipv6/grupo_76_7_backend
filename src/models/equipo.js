const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const equipoSchema = new Schema({
    linea: {
        type: String
    },
    modelo: {
        type: String
    },
    marca: {
        type: String
    },
    cantidadPuertos: {
        type: Number
    }
}, {
    collection: 'equipos'
});

module.exports = mongoose.model('Equipo', equipoSchema);