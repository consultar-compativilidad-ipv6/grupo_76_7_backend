const express = require('express');
const EquipoController = require('../controllers/equipoController');

class EquipoRouter {
    constructor() {
        this.router = express.Router();
        this.config();
    }

    config() {
        const objEquipoC = new EquipoController();
        this.router.post("/equipo", objEquipoC.registrar);
        this.router.get("/equipo", objEquipoC.getEquipo);
        this.router.get('/equipo/:id', objEquipoC.getEquipoById);
        this.router.put("/equipo", objEquipoC.putEquipo);
        this.router.delete("/equipo", objEquipoC.delete);
    }
}

module.exports = EquipoRouter;