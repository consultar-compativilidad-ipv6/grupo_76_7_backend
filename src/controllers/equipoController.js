const equipo = require('../models/equipo');

class EquipoController {
    constructor() {

    }

    registrar(req, res) {
        equipo.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data)
            }
        });
    }

    getEquipo(req, res) {
        equipo.find((error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data)
            }
        })
    }

    getEquipoById(req, res) {
        let id = req.params.id;
        equipo.findById(id, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        });
    }


    putEquipo(req, res) {
        let { _id, linea, modelo, marca, cantidadPuertos } = req.body;
        let obj = {
            linea, modelo, marca, cantidadPuertos
        }   
        equipo.findByIdAndUpdate(_id, { $set: obj }, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data)
            }
        })
    }

    delete(req, res) {
        let { id } = req.body;
        equipo.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data)
            }
        })
    }
}

module.exports = EquipoController;